FROM tomcat:8.0.36-jre8

#COPY WAR
ADD target/Autocomplete.war /usr/local/tomcat/webapps/

#ADD TOMCAT USER CONF
ADD tomcat-users.xml /usr/local/tomcat/conf/

EXPOSE 8080
CMD ["catalina.sh", "run"]