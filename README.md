#README 

#INTRODUCTION
The aim of this example is to build an autocomplete functionality.

* Have a default file "Word.lst" which has the default entries of the dictionary.
* Being able to add or delete entries in the dictionary droping a file in a folder.

##FRONTEND FUNCTIONALITY

The fronted is quite simple. There is and input text box to and a select input box with the search results. The engine starts searching from the 3rd letter entered in the input text box.

###Fronted Technologies
* Bootstrap 
* BackBone 
* JQuery

##BACKEND FUNCTIONALITY

The backend has to main functionalities:
* Maintain the entries of the dictionary.
    * Clean all the entries of the dictionary and inserts the default ones every 24 hours. We achieve this functionality using spring scheduled task.
    * Ability to add or remove entries at any time using a simple txt file. Basically, using Apache Camel we can either drop a file like "add_words*.txt" or "delete_words*.txt" in the camelWords folder to add or remove entries.
* Perform a search matching the input entered in the frontend.

** Database **
As this is an example we are using and embedded H2 database.

###BackEnd Technologies 
* Using SpringMVC
* Spring-batch 
* Spring-data 
* Hibernate
* Camel
* MockMvc testing with Mockito

#APPENDIX

##Docker useful commands
* Build the image -> sudo docker build -t kimy82/autocomplete .
* Run the image -> sudo docker run -it --rm -p 8080:8080 kimy82/autocomplete
* Add new words to the dictionary ->  sudo docker cp add_words.txt containerId:usr/local/tomcat/camelWords
* Example for copy logs -> sudo docker cp containerId:logs/manager.2016-06-27.log /
* Run docker image -> sudo docker run -it --rm -p 8080:7070 kimy82/autocomplete

##Camel
There are 2 main routes configured. The first one matches files like add_words*.txt and it is used for adding words to the dictionary. The second one is delete_words*.txt and it is used for removing words from the dictionary.
The camel folder where we should drop these files is located in System property user.dir which within docker container is in /usr/local/tomcat/camelWords.

##Schema.sql
Schema.sql file can be used for performing inserts when the application starts.

##Run without docker
If you wish to run the project without docker there are two options. The first one might be to download the repository, run _mvn install_ and once the war file is built use a tomcat container for run it. The second one can be using the jetty embedded server that is available in the project. In that case just run _mvn jetty:run_.


