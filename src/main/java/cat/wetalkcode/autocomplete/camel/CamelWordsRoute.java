package cat.wetalkcode.autocomplete.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel route that picks file from camel folder and processes new entries of Dictionary.
 * 
 * @author kim
 *
 */
@Component(value="camelWordsRoute")
public class CamelWordsRoute extends RouteBuilder {
	
	protected static String CLASSPATH_WORD_LST = System.getProperty("user.dir") + "/webapps/Autocomplete/camelWords/";
	
	@Autowired
	private CamelAddWordsProcessor camelAddWordsProcessor;
	
	@Autowired
	private CamelDeleteWordsProcessor camelDeleteWordsProcessor;
	
    @Override
    public void configure() throws Exception {
    	System.out.println("Dis ::::: " + System.getProperty("user.dir"));
        from("file:" + CLASSPATH_WORD_LST + "?include=add_words*.txt&autoCreate=true&move=done" )
        	.split(body().tokenize("\n"))
        	.streaming()
        .process(camelAddWordsProcessor);
        
        from("file:" + CLASSPATH_WORD_LST + "?include=delete_words*.txt&autoCreate=true&move=done" )
    		.split(body().tokenize("\n"))
    		.streaming()
    	.process(camelDeleteWordsProcessor);
    
        
    }
 
}