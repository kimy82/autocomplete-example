package cat.wetalkcode.autocomplete.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;
import cat.wetalkcode.autocomplete.entities.Dictionary;

/**
 * Processes entry of Dictionary.
 * 
 * @author kim
 *
 */
@Component
public class CamelAddWordsProcessor implements Processor {

	@Autowired
	private DictionaryDAO dictionaryDAO;

	public void process(Exchange exchange) throws Exception {
		
		String data = exchange.getIn().getBody(String.class);
		saveEntry(data);

	}

	private void saveEntry(String entry) {
		
		String trimedEntry = entry.trim();
		if (this.dictionaryDAO.findByKey(trimedEntry).isEmpty())
			this.dictionaryDAO.save(new Dictionary(trimedEntry));

	}
}