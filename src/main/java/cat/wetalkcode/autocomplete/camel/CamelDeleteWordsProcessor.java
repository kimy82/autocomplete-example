package cat.wetalkcode.autocomplete.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;

/**
 * Processes entry of Dictionary.
 * 
 * @author kim
 *
 */
@Component
public class CamelDeleteWordsProcessor implements Processor {

	@Autowired
	private DictionaryDAO dictionaryDAO;

	public void process(Exchange exchange) throws Exception {
		
		String data = exchange.getIn().getBody(String.class);
		deleteEntry(data);

	}

	private void deleteEntry(String entry) {

			this.dictionaryDAO.delete(entry.trim());

	}
}