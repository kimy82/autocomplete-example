package cat.wetalkcode.autocomplete.services.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.exceptions.ValorsException;
import cat.wetalkcode.autocomplete.services.DatabaseGenerator;

/**
 * 
 * @author kim
 *
 */
@Component
public class DatabaseGeneratorImpl implements DatabaseGenerator {

	protected static String CLASSPATH_WORD_LST = "classpath:word.lst";

	@Autowired
	private DictionaryDAO dictionaryDAO;

	/**
	 * Scheduled task that every day at 1am and rebuilds the dictionary with default entries.<br>
	 *
	 */
	@Scheduled(initialDelay=2000, fixedRate=86400000)
	public void cleanDictionary() throws FileNotFoundException {

		BufferedReader br = getBufferReaderFromFile(CLASSPATH_WORD_LST);

		this.dictionaryDAO.deleteAll();

		saveEntries(br);

	}

	private BufferedReader getBufferReaderFromFile(String filePath)
			throws FileNotFoundException {

		File file = ResourceUtils.getFile(filePath);
		InputStream inputStream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(
				inputStream));
		return br;

	}

	private void saveEntries(BufferedReader br) {

		String line;
		try {
			while ((line = br.readLine()) != null) {
				this.dictionaryDAO.save(new Dictionary(line.trim()));
			}
		} catch (IOException e) {
			throw new ValorsException("Error in reading line", e);
		}
		try {
			br.close();
		} catch (IOException e) {
			throw new ValorsException("Error in closing stream", e);
		}
	}
}
