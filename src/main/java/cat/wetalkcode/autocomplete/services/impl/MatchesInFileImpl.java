package cat.wetalkcode.autocomplete.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;
import cat.wetalkcode.autocomplete.dto.BaseResponse;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.services.MatchesInFile;

/**
 * 
 * @author kim
 *
 */
@Service
public class MatchesInFileImpl implements MatchesInFile {

	@Autowired
	private DictionaryDAO dictionaryDAO;

	/**
	 * Search in DB for any world containing a key
	 * 
	 * @param key
	 *            to search in DB
	 *@return list of {@link BaseResponse}
	 */
	public List<BaseResponse> findMatchesByKey(String key) {

		List<Dictionary> dictionaries = this.dictionaryDAO.findByKeyLike("%" + key + "%");
		List<BaseResponse> baseResponses = getBaseResponseList(dictionaries);
		
		return baseResponses;
	}

	private  List<BaseResponse> getBaseResponseList(List<Dictionary> dictionaries) {
		List<BaseResponse> baseResponses = new ArrayList<BaseResponse>();
		
		for (Dictionary dictionary : dictionaries) {
			BaseResponse baseResponse = new BaseResponse();
			BeanUtils.copyProperties(dictionary, baseResponse);
			baseResponses.add(baseResponse);
		}
		return baseResponses;
	}
}
