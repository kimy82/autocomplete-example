package cat.wetalkcode.autocomplete.services;

import java.io.FileNotFoundException;

/**
 * 
 * Clean entries in DB.
 * 
 * @author kim
 *
 */
public interface DatabaseGenerator {

	void cleanDictionary() throws FileNotFoundException;
	
}
