package cat.wetalkcode.autocomplete.services;

import java.util.List;

import cat.wetalkcode.autocomplete.dto.BaseResponse;
import cat.wetalkcode.autocomplete.entities.Dictionary;

/**
 * Service that handles the search in DB for {@link Dictionary}any world
 * containing a key
 * 
 * @author kim
 *
 */
public interface MatchesInFile {

	List<BaseResponse> findMatchesByKey(String key);
}
