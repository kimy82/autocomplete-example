package cat.wetalkcode.autocomplete.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cat.wetalkcode.autocomplete.entities.Dictionary;

/**
 * Spring Data Repository for CRUD Operations on {@link Dictionary}
 * 
 * @author kim
 *
 */
@Repository
public interface DictionaryRepository extends CrudRepository<Dictionary, Long> {

	List<Dictionary> findByKeyLikeIgnoreCase(String key);

	List<Dictionary> findByKey(String key);
	
	void deleteByKey(String key);
}
