package cat.wetalkcode.autocomplete.dao;

import java.util.List;

import cat.wetalkcode.autocomplete.entities.Dictionary;

/**
 * Handles operations on {@link #Dictionary}
 * 
 * @author kim
 *
 */
public interface DictionaryDAO {

	List<Dictionary> findByKey(String key);
	
	List<Dictionary> findByKeyLike(String key);

	void save(Dictionary dictionary);

	void deleteAll();
	
	void delete(String key);

}
