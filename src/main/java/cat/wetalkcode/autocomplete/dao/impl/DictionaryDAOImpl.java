package cat.wetalkcode.autocomplete.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.repositories.DictionaryRepository;

@Service
public class DictionaryDAOImpl implements DictionaryDAO {

	@Autowired
	private DictionaryRepository dictionaryRepository;

	@Transactional(readOnly=true)
	public List<Dictionary> findByKey(String key) {
		return this.dictionaryRepository.findByKey(key);
	}
	
	@Transactional(readOnly=true)
	public List<Dictionary> findByKeyLike(String key) {
		return this.dictionaryRepository.findByKeyLikeIgnoreCase(key);
	}

	@Transactional
	public void save(Dictionary dictionary) {
		this.dictionaryRepository.save(dictionary);
	}
	
	@Transactional
	public void deleteAll() {
		this.dictionaryRepository.deleteAll();
	}
	
	@Transactional
	public void delete(String key) {
		this.dictionaryRepository.deleteByKey(key);
	}
}
