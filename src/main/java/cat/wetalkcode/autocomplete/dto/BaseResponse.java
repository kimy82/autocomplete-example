package cat.wetalkcode.autocomplete.dto;

/**
 * DTO for Dictionary entries
 * 
 * @author kim
 *
 */
public class BaseResponse {

    private String key;

    public BaseResponse() {
    }

    public BaseResponse(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
