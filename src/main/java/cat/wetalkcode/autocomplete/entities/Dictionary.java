package cat.wetalkcode.autocomplete.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity that holds entries from the dictionary.
 * 
 * @author kim
 *
 */
@Entity
@Table(name="dictionary")
public class Dictionary {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(name="keyvalor")
	private String key;

	public Dictionary(String key) {
		this.key = key;
	}
	
	public Dictionary() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
}
