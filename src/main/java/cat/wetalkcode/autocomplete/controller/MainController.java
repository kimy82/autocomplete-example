package cat.wetalkcode.autocomplete.controller;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cat.wetalkcode.autocomplete.dto.BaseResponse;
import cat.wetalkcode.autocomplete.services.MatchesInFile;

/**
 * Controller for Autocomplete app.
 * @author Kim
 *
 */
@Controller
public class MainController {

	@Autowired
	private MatchesInFile matchesInFile;

	/**
	 * Holds starting point of application
	 * 
	 * @param name
	 * @param model
	 * @return
	 */
	@RequestMapping({ "/", "/index.html", "/index.htm" })
	public String hello(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
		model.addAttribute("name", name);
		return "index";
	}

	/**
	 * Example of an Ajax call You can invoke this method by calling
	 * /testJson.json?key=xxxx
	 * 
	 * @param key
	 * @return
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/testJson.json")
	public @ResponseBody List<BaseResponse> testJson(@RequestParam(value = "key") String key) throws FileNotFoundException {
		
		List<BaseResponse> responses = new ArrayList<BaseResponse>();

		if(!"".equals(key)){
			responses = matchesInFile.findMatchesByKey(key);
		}
		return responses;
	}
}
