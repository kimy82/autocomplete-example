package cat.wetalkcode.autocomplete.exceptions;

public class ValorsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ValorsException(String message, Exception e) {
		super(message, e);
	}

}
