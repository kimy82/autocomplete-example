<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Page</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/index.css" />" />
</head>
<body id="dictionary">
        <div class="container">
            <h1  class="text-center text-uppercase">Dictionary Lookup <span class="glyphicon glyphicon-search"></span></h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div id="input-feedback" class="form-group">
                        <label for="search">Input your search term</label>
                        <input type="text" id="search" class="form-control" placeholder="Search..">
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="optionsKey">Our suggestions</label>
                        <select id="optionsKey" class="form-control" disabled>
                            
                        </select>
                    </div>
                </div>
            </div>
        </div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<!-- Bacbone JS -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js" type="text/javascript"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js" type="text/javascript"></script>

	<script src="<c:url value="/resources/js/index.js" />"></script>
</body>
</html>