var app = {}; // create namespace for our app

/**
 * Model for options
 */
app.BaseResponse = Backbone.Model.extend({
	defaults : {
		key : ''
	}
});

/**
 * Collection of BaseResponses
 */
app.BaseResponseList = Backbone.Collection.extend({
	defaults : {
		key : 'some key'
	},
	model : app.BaseResponse,
	url : function() {
		return 'testJson.json?key=' + this.defaults.key;
	},
	initialize : function() {
		this.fetch();
	},
});

/**
 * View for the page.
 */
app.BaseResponseView = Backbone.View.extend({
	el : '#dictionary',
	initialize : function() {
		this.listenTo(app.baseResponseList, 'sync', this.render);
		 $('#search').keyup(this.fetchNewList);
	},
	fetchNewList : function(){
		app.baseResponseView.removeFeedBack();
		if(this.value.length > 2){			
			app.baseResponseList.defaults.key = this.value;
			app.baseResponseList.fetch();
		}else{
			app.baseResponseView.$el.find("#optionsKey").empty().attr("disabled","disabled");
			app.baseResponseView.removeFeedBack();
			
		}
	},
	render : function() {
		var el = this.$el.find("#optionsKey");
		el.empty();
		
		if(app.baseResponseList.models.length > 0)
			this.addSuccess();
		else if(this.$el.find('#input-feedback input').val() != '')
			this.addFailure();
		
		_.each(app.baseResponseList.models, function(option){
			el.append('<option value="' + option.attributes.key + '">' + option.attributes.key
					+ '</option>'); 
		});
	},
	removeFeedBack : function(){
		app.baseResponseView.$el.find('#input-feedback').removeClass('has-error has-success');
		app.baseResponseView.$el.find('#input-feedback .glyphicon').removeClass('glyphicon-remove glyphicon-ok');
	},
	addSuccess : function(){
		this.$el.find('#input-feedback').addClass('has-success');
		this.$el.find('#input-feedback .glyphicon').addClass('glyphicon-ok');
		this.$el.find("#optionsKey").removeAttr("disabled");
	},
	addFailure : function(){
		this.$el.find('#input-feedback').addClass('has-error');
		this.$el.find('#input-feedback .glyphicon').addClass('glyphicon-remove');
		this.$el.find("#optionsKey").attr("disabled","disabled");
	}
});

app.baseResponseList = new app.BaseResponseList();
app.baseResponseView = new app.BaseResponseView();
