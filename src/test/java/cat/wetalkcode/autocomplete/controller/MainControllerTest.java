package cat.wetalkcode.autocomplete.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import cat.wetalkcode.autocomplete.controller.MainController;
import cat.wetalkcode.autocomplete.dto.BaseResponse;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "/dispatcher-servlet.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class MainControllerTest {

	@Autowired
	private MainController mainController;

	private static final String nameTest = "nameTest";

	private MockMvc mockMvc;

	private ObjectMapper om = new ObjectMapper();

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(mainController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
	}

	@Test
	public void helloReturnsIndex() {

		Model mockModel = Mockito.mock(Model.class);
		Assert.assertThat(this.mainController.hello("nameTest", mockModel), is(equalTo("index")));
		Mockito.verify(mockModel).addAttribute("name", nameTest);
	}

	@Test
	@DatabaseSetup(value = "/cat/wetalkcode/autocomplete/controller/entries.xml", type = DatabaseOperation.INSERT)
	@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "/cat/wetalkcode/autocomplete/controller/entries.xml")
	public void testJsonReturnsBaseResponseWithCorrectKeyAllResults() throws Exception {

		MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/testJson.json").param("key", "key"))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		String jsonArray = result.getResponse().getContentAsString();

		BaseResponse[] asArray = om.readValue(jsonArray, BaseResponse[].class);
		List<String> stringEntries = new ArrayList<String>();
		for (BaseResponse dictionary : asArray) {
			stringEntries.add(dictionary.getKey());
		}
		Assert.assertThat(stringEntries, Matchers.contains("key1", "key2", "key3", "key33", "key993366"));

	}

	@Test
	@DatabaseSetup(value = "/cat/wetalkcode/autocomplete/controller/entries.xml", type = DatabaseOperation.INSERT)
	@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "/cat/wetalkcode/autocomplete/controller/entries.xml")
	public void testJsonReturnsBaseResponseWithCorrectKeyWithKey33Matches() throws Exception {

		MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/testJson.json").param("key", "33"))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		String jsonArray = result.getResponse().getContentAsString();

		BaseResponse[] asArray = om.readValue(jsonArray, BaseResponse[].class);
		List<String> stringEntries = new ArrayList<String>();
		for (BaseResponse dictionary : asArray) {
			stringEntries.add(dictionary.getKey());
		}
		Assert.assertThat(stringEntries, Matchers.contains("key33", "key993366"));

	}

}
