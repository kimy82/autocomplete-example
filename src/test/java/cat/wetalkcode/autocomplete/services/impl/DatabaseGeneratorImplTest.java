package cat.wetalkcode.autocomplete.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import cat.wetalkcode.autocomplete.dao.DictionaryDAO;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.services.impl.DatabaseGeneratorImpl;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseGeneratorImplTest {

	@InjectMocks
	private DatabaseGeneratorImpl databaseGeneratorImpl;

	@Mock
	private DictionaryDAO dictionarydao;

	@Captor
	private ArgumentCaptor<Dictionary> dictionaryCaptor;

	@Test
	public void databasseGeneratorCallsDeleteAllAndThenSaveDictionaryEntries() throws Exception {
	
		DatabaseGeneratorImpl.CLASSPATH_WORD_LST= "classpath:word_test.lst";
		
		this.databaseGeneratorImpl.cleanDictionary();
		
		InOrder inorder = Mockito.inOrder(this.dictionarydao);
		inorder.verify(this.dictionarydao).deleteAll();
		inorder.verify(this.dictionarydao, Mockito.times(6)).save(this.dictionaryCaptor.capture());

		List<Dictionary> dictionaryEntries = this.dictionaryCaptor.getAllValues();
		List<String> stringEntries = dictionaryEntries.stream().map(Dictionary::getKey).collect(Collectors.toList());
		
		Assert.assertThat(stringEntries, Matchers.contains("aa", "aah", "aahed", "aahing", "aahs", "aal"));
		
	}
}
