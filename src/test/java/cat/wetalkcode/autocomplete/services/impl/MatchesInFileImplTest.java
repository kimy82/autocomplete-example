package cat.wetalkcode.autocomplete.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import cat.wetalkcode.autocomplete.dao.impl.DictionaryDAOImpl;
import cat.wetalkcode.autocomplete.dto.BaseResponse;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.services.impl.MatchesInFileImpl;
import static java.util.stream.Collectors.toList;

@RunWith(MockitoJUnitRunner.class)
public class MatchesInFileImplTest {

	private static final String KEY = "key";
	
	private List<Dictionary> dictionaryList = new ArrayList<Dictionary>();

	@InjectMocks
	private MatchesInFileImpl matchesInFileImpl;
	
	@Mock
	private DictionaryDAOImpl dictionaryDAO;
	
	@Before
	public void setUp(){
		dictionaryList = Arrays.asList(new Dictionary("key1"), new Dictionary("key2"), new Dictionary("key3"));
	}
	
	@Test
	public void matchesInFileReturnsCorrectList() throws Exception {
		
		Mockito.when(this.dictionaryDAO.findByKeyLike("%" + KEY + "%")).thenReturn(dictionaryList);
		List<BaseResponse> responses = this.matchesInFileImpl.findMatchesByKey(KEY);
		
		List<String>  stringEntries = responses.stream().map(BaseResponse::getKey).collect(toList());

		Assert.assertThat(stringEntries, Matchers.contains("key1","key2","key3") );
	}
}
