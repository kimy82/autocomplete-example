package cat.wetalkcode.autocomplete.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import cat.wetalkcode.autocomplete.dao.impl.DictionaryDAOImpl;
import cat.wetalkcode.autocomplete.entities.Dictionary;
import cat.wetalkcode.autocomplete.repositories.DictionaryRepository;

@RunWith(MockitoJUnitRunner.class)
public class DictionaryDAOImplTest {

	private static final String KEY = "key";

	@InjectMocks
	private DictionaryDAOImpl dictionaryDAO;
	
	@Mock
	private DictionaryRepository dictionaryRepository;
	
	@Test
	public void deleteAllCallsRepositoryDeleteAll() throws Exception {
		this.dictionaryDAO.deleteAll();
		Mockito.verify(this.dictionaryRepository).deleteAll();
	}
	
	@Test
	public void findByKeyCallRepository() throws Exception {
		this.dictionaryDAO.findByKey(KEY);
		Mockito.verify(this.dictionaryRepository).findByKey(KEY);
	}
	
	@Test
	public void saveCallRepositorySave() throws Exception {
		Dictionary dictionary = new Dictionary();
		this.dictionaryDAO.save(dictionary);
		Mockito.verify(this.dictionaryRepository).save(dictionary);
	}
	
}
